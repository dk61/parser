# Class requirements checklist

- constructor is implemented
- initialize state in constructor
- no setters for private fields
- no external changes for private field 
- no static methods
- one abstraction level per routine
- purpose (__one!__) of class/method/field/temp variable is fully explained by its name
- there is no code with repeated logic (DRY)
- every stream is closed after manipulations with it
- every method uses a cheapest algorythm to get result
