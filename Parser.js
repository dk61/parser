const fs = require("fs");

class Parser {
  file;

  setFile(f) {
    this.file = f;
  }

  getFile() {
    return this.file;
  }

  getContent() {
    return new Promise((res) => {
      fs.open(this.file, (err, fd) => {
        const b = Buffer.alloc(1);
        let output = "";
        while (fd > 0) {
          const n = fs.readSync(fd, b, 0, 1, null);
          if (n === 0) break;
          output += String.fromCharCode(b[0]);
        }
        res(output);
      });
    });
  }

  getContentWithoutUnicode() {
    return new Promise((res) => {
      fs.open(this.file, (err, fd) => {
        const b = Buffer.alloc(1);
        let output = "";
        while (fd > 0) {
          const n = fs.readSync(fd, b, 0, 1, null);
          if (n === 0) break;
          if (b[0] < 0x80) {
            output += String.fromCharCode(b[0]);
          }
        }
        res(output);
      });
    });
  }

  saveContent(content) {
    return new Promise((res) => {
      const o = fs.createWriteStream(this.file);

      for (let i = 0; i < content.length; i += 1) {
        o.write(content[i], (err) => err && console.log({ err }));
      }
      o.close(res);
    });
  }
}

module.exports = { Parser };
