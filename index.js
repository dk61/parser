const { Parser } = require("./Parser");

const main = async () => {
  const parser = new Parser();
  parser.setFile("file.txt");
  console.log({ common: await parser.getContent() });
  console.log({ common: await parser.getContent() });
  console.log({ common: await parser.getContent() });
  console.log({ common: await parser.getContent() });
  await parser.saveContent("new\n\tcontent\n123sdsd\t\n\t\nööö");
  console.log({ savedWithoutUnicode: await parser.getContentWithoutUnicode() });
  console.log({ saved: await parser.getContent() });
};

main();
